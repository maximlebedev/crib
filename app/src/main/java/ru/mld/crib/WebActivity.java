package ru.mld.crib;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

public class WebActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_html);

        String link = getIntent().getExtras().getString("link");
        callCat(link);
    }

    public void callCat(String link) {
        WebView webView = findViewById(R.id.webView);
        webView.loadUrl(link);
    }
}
