package ru.mld.crib;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView listOfCatsBreeds = findViewById(R.id.listView);
        String[] catsBreeds = getResources().getStringArray(R.array.catsBreeds);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.rowlayout, catsBreeds);
        listOfCatsBreeds.setAdapter(adapter);

        listOfCatsBreeds.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id) {

                Intent intent = new Intent(MainActivity.this, WebActivity.class);
                String[] catsBreedsLinks = getResources().getStringArray(R.array.catsBreedsLinks);
                intent.putExtra("link",catsBreedsLinks[position]);
                startActivity(intent);
            }
        });
    }
}